#!/bin/bash

# Script que analiza el fichero de texto generado por una operacion con un numero de registros
# determinado y obtiene una valor unico a traves de una media que almacena en un csv
# para posteriores comparaciones.

nom=$1
op=${nom:3:1}
registros="${nom%%.*}"
registros="${registros#*-}"
TOTAL=0
num=0
MEDIA=0

sed -i "1,2d" $1	# Borramos cabecera iostat
sed -i '/sda/!d' $1	# Nos quedamos con los valores numéricos
sed -i 's/,/./g' $1	# Cambiamos ',' por '.' para operar con bc

# Nos quedamos con la columna $2 correspondiente a tps y vamos creando una cifra total para despues hacer la media
for i in $( awk '{ print $2; }' $1)
do
	TOTAL=$(echo $TOTAL+$i | bc )
	let num=$num+1;
done

MEDIA=$(awk "BEGIN {printf ${TOTAL}/${num}}")
let registros=$registros+0; # Por alguna extraña razon no me escribe registros si no hago esto
echo $op,$registros,$MEDIA >> disk.csv
