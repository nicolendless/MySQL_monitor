#!/bin/bash

# Script que monitoriza la carga que supone para un sistema la ejecucion de una serie
#	de operaciones SQL.
# El usuario elige el numero de registros inicial (registros) que se iran aumentando un
# numero de veces (numEjecuciones) multiplicando el valor inicial por una cifra elegida (difEjecuciones).
# Por ejemplo registros=10, numEjecuciones=3, difEjecuciones=10 tendremos una monitorizacion con 10, 100 y 1000 registros.

ejecuciones=0
registros=$1
numEjecuciones=$2
difEjecuciones=$3
ops=0

while [ $ejecuciones -lt $numEjecuciones ]; do
	echo "> Creación $registros registros"
	./insertDB.sh $registros	#insercion $registros registros
	for ((i=ops; i<=ops; i++))
	do
		echo "> Ejecución op$i con $registros registros"
		./op$i.sh & # ejecutamos la operacion y la lanzamos a background
		proc1=$!
		# lanzamos las operaciones de monitorizacion y guardamos el output en un fichero
		top -b -u mysql -d 1 | grep --line-buffered mysqld | tee cpu$i-$registros.txt > mem$i-$registros.txt &
		proc2=$!
		iostat -y 1 10000 > dis$i-$registros.txt &
		proc3=$!
		# esperamos a que finalice la operacion lanzada para detener la monitorizacion
		wait "$proc1"
		kill "$proc2"
		kill "$proc3"
	done
	# ejecutadas todas las operaciones para ese numero de registros, aumentamos la cantidad
	# para hacer lo mismo con un numero mayor
	let ejecuciones=$ejecuciones+1;
	let registros=$registros*difEjecuciones;
	./deleteDB.sh	#borrar registros DB
	echo "> Borrar BD"
	echo ""
done

ejecuciones=0
registros=$1
numEjecuciones=$2
difEjecuciones=$3

# A continuacion debemos analizar todos los resultados obtenidos y observar
# los comportamientos de la cpu, memoria y disco independientemente.
# Como se han guardado los ficheros con un nombre distintivo, p.e: cpu$i-$registros.txt
# donde $i es el numero de operacion y $registros la cantidad de registros que se han
# creado para estresar el sistema, basta con hacer un while como el anterior.

while [ $ejecuciones -lt $numEjecuciones ]; do
	for ((i=ops; i<=ops; i++))
	do
		echo ">	Analizando resultados de CPU"
		./analizar_CPU.sh cpu$i-$registros.txt
		echo ">	Analizando resultados de MEMORIA"
		./analizar_MEM.sh mem$i-$registros.txt
		echo ">	Analizando resultados de DISCO"
		./analizar_DISK.sh dis$i-$registros.txt
	done
	let ejecuciones=$ejecuciones+1;
	let registros=$registros*difEjecuciones;
done
