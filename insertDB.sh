#!/bin/bash
NUMREG=$1
i=0
rm -f insert.txt 
echo "ALTER TABLE Persona AUTO_INCREMENT = 1;" >> insert.txt
echo "ALTER TABLE Persona ADD INDEX (Tel);" >> insert.txt
while [ $i -lt $NUMREG ]; do
	echo "INSERT INTO Persona (NomP, Edad, Tel)
	VALUES (conv(floor(rand() * 99999999999999), 20, 36), floor(rand() * (100) + 1), floor(rand() * (100000) + 1));" >> insert.txt

	let i=$i+1
done

i=0
echo "ALTER TABLE Coche AUTO_INCREMENT = 1;" >> insert.txt
echo "ALTER TABLE Coche ADD INDEX (Ref);" >> insert.txt
while [ $i -lt $NUMREG ]; do
	echo "INSERT INTO Coche (NomC, Persona, Matr, Ref)	
	VALUES (conv(floor(rand() * 99999999999999), 20, 36), floor(rand() * ($NUMREG) + 1), floor(rand() * (100000) + 1), conv(floor(rand() * 99999999999999), 20, 36));" >> insert.txt
	let i=$i+1 
done

i=0
echo "ALTER TABLE Ruta AUTO_INCREMENT = 1;" >> insert.txt
echo "ALTER TABLE Ruta ADD INDEX (Num);" >> insert.txt
while [ $i -lt $NUMREG ]; do
	echo "INSERT INTO Ruta (NomR, Coche, Num, Km)	
	VALUES (conv(floor(rand() * 99999999999999), 20, 36), floor(rand() * ($NUMREG) + 1), floor(rand() * (100000) + 1), floor(rand() * (100000) + 1));" >> insert.txt
	let i=$i+1
done

mysql acsi < insert.txt

