#!/bin/bash
nom=$1
op=${nom:3:1}
registros="${nom%%.*}"
registros="${registros#*-}"
TOTAL=0
num=0
MEDIA=0

#sed -i "1,2d" $1	# Borramos cabecera iostat
#sed -i '/^    */!d' $1	# Nos quedamos con los valores numéricos
sed -i 's/,/./g' $1	# Cambiamos ',' por '.' para operar con bc

# Nos quedamos con la columna $4 correspondiente a %iowait
for i in $( awk '{ print $10; }' $1)
do 		
	TOTAL=$(echo $TOTAL+$i | bc )
	let num=$num+1;
done
#MEDIA=$(echo "scale=2; $TOTAL / $num" | bc)
MEDIA=$(awk "BEGIN {printf ${TOTAL}/${num}}")
let registro=$registros+0; # Por alguna extraña razon no me escribe registros si no hago esto
echo $op,$registros,$MEDIA >> mem.csv
#rm -f $1
