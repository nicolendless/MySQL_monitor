#!/bin/bash
TOTAL=0
MEDIA=0
NUM_E=20
for ((i=0; i<$NUM_E; i++))
do	
	#Calculamos el total de CPU = tusu + tsys
	num=$(echo $( TIMEFORMAT="%3U + %3S"; { time mysql acsi -e "SELECT Persona.NomP, Coche.ID FROM Persona
		INNER JOIN Coche on Persona.ID=Coche.ID;" >/dev/null 2>&1; } 2>&1) | bc -l)
	#Vamos sumando todos los tCPU
	TOTAL=$(echo $TOTAL+$num | bc)	
done
MEDIA=$(awk "BEGIN {printf ${TOTAL}/${NUM_E}}")
echo -n $MEDIA >> valores.csv 
