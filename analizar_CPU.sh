#!/bin/bash

# Script que analiza el fichero de texto generado por una operacion con un numero de registros
# determinado y obtiene una valor unico a traves de una media que almacena en un csv
# para posteriores comparaciones.

nom=$1
op=${nom:3:1}	# sacamos el numero de operacion del nombre del fichero
registros="${nom%%.*}"
registros="${registros#*-}" # sacamos el numero de registros del nombre del fichero
TOTAL=0
num=0
MEDIA=0

sed -i 's/,/./g' $1	# Cambiamos ',' por '.' para operar con bc

# recorremos todos los valores de la novena columna que es el valor qe nos interesa
# y vamos creando una cifra total para despues hacer la media
for i in $( awk '{ print $9; }' $1)
do
	TOTAL=$(echo $TOTAL+$i | bc )
	let num=$num+1;
done

MEDIA=$(awk "BEGIN {printf ${TOTAL}/${num}}")
let registro=$registros+0; # "Arreglillo" porque no dejaba escribir el valor $registros (puede que al ser un string)
echo $op,$registros,$MEDIA >> cpu.csv
